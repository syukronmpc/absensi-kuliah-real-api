<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	// Dosen
	public function dosen_list() {
		try {
			$kampus_id = $this->input->get('kampus_id');

			if ($kampus_id !== null) {
				$this->db->where('dosen.kampus_id', $kampus_id);
			}

			$this->db->select('*, dosen.id as `id`, user.password, dosen.nama as `nama`, kampus.nama as `kampus`', FALSE);
			$this->db->from('dosen');
			$this->db->join('kampus', 'kampus.id = dosen.kampus_id', 'inner');
			$this->db->join('user', 'user.id = dosen.user_id', 'inner');
			$query = $this->db->get();

			$dosens = $query->result_array();

			echo $this->service->successResponse($dosens);
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}

	public function dosen_add() {
		try {
			$id = $this->input->post('id');
			$kampus_id = $this->input->post('kampus_id');
			$password = $this->input->post('password');
			$gender = $this->input->post('gender');
			$nama = $this->input->post('nama');
			$email = $this->input->post('email');
			$no_hp = $this->input->post('no_hp');

			$userdata = [
				'username' => $id,
				'password' => $password,
				'akses' => $this->service->akses['DOSEN'],
			];

			$insert = $this->service->insert('user', $userdata);

			if ($insert) {
				$data = [
					'id' => $id,
					'kampus_id' => $kampus_id,
					'gender' => $gender,
					'nama' => $nama,
					'email' => $email,
					'no_hp' => $no_hp,
					'user_id' => $this->db->insert_id()
				];

				$this->service->insert('dosen', $data);
				echo $this->service->successResponse([], 'Tambah data dosen berhasil!');
			}
			
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}

	public function dosen_update() {
		try {
			$id = $this->input->post('id');

			$default = $this->service->detail('dosen', 'id', $id);

			$password = $this->input->post('password') ?? $default['password'];
			$gender = $this->input->post('gender') ?? $default['gender'];
			$nama = $this->input->post('nama') ?? $default['nama'];
			$email = $this->input->post('email') ?? $default['email'];
			$no_hp = $this->input->post('no_hp') ?? $default['no_hp'];

			$userdata = [
				'password' => $password,
			];

			$whereUser = ['id' => $default['user_id']];

			$update = $this->service->update('user', $whereUser, $userdata);

			if ($update) {
				$data = [
					'gender' => $gender,
					'nama' => $nama,
					'email' => $email,
					'no_hp' => $no_hp,
				];

				$where = ['id' => $id,];

				$this->service->update('dosen', $where, $data);
				echo $this->service->successResponse([], 'Update data dosen berhasil!');
			}
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}

	public function dosen_delete() {
		try {
			$id = $this->input->post('id');

			$default = $this->service->detail('dosen', 'id', $id);

			$whereUser = ['id' => $default['user_id']];
			$whereAdmin = ['id' => $id];

			$this->service->delete('user',$whereUser);
			$this->service->delete('dosen',$whereAdmin);
			
			echo $this->service->successResponse([], 'Data dosen berhasil di hapus!');
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}
	// End Of Dosen

	// Mahasiswa
	public function mahasiswa_list() {
		try {
			$kampus_id = $this->input->get('kampus_id');

			if ($kampus_id !== null) {
				$this->db->where('mahasiswa.kampus_id', $kampus_id);
			}

			$this->db->select('*, mahasiswa.id as `id`, user.password, mahasiswa.nama as `nama`, kampus.nama as `kampus`', FALSE);
			$this->db->from('mahasiswa');
			$this->db->join('kampus', 'kampus.id = mahasiswa.kampus_id', 'inner');
			$this->db->join('user', 'user.id = mahasiswa.user_id', 'inner');
			$query = $this->db->get();

			$mahasiswas = $query->result_array();

			echo $this->service->successResponse($mahasiswas);
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}

	public function mahasiswa_add() {
		try {
			$id = $this->input->post('id');
			$kampus_id = $this->input->post('kampus_id');
			$password = $this->input->post('password');
			$gender = $this->input->post('gender');
			$nama = $this->input->post('nama');
			$email = $this->input->post('email');
			$no_hp = $this->input->post('no_hp');

			$userdata = [
				'username' => $id,
				'password' => $password,
				'akses' => $this->service->akses['MAHASISWA'],
			];

			$insert = $this->service->insert('user', $userdata);

			if ($insert) {
				$data = [
					'id' => $id,
					'kampus_id' => $kampus_id,
					'gender' => $gender,
					'nama' => $nama,
					'email' => $email,
					'no_hp' => $no_hp,
					'user_id' => $this->db->insert_id()
				];

				$this->service->insert('mahasiswa', $data);
				echo $this->service->successResponse([], 'Tambah data mahasiswa berhasil!');
			}
			
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}

	public function mahasiswa_update() {
		try {
			$id = $this->input->post('id');

			$default = $this->service->detail('mahasiswa', 'id', $id);

			$password = $this->input->post('password') ?? $default['password'];
			$gender = $this->input->post('gender') ?? $default['gender'];
			$nama = $this->input->post('nama') ?? $default['nama'];
			$email = $this->input->post('email') ?? $default['email'];
			$no_hp = $this->input->post('no_hp') ?? $default['no_hp'];

			$userdata = [
				'password' => $password,
			];

			$whereUser = ['id' => $default['user_id']];

			$update = $this->service->update('user', $whereUser, $userdata);

			if ($update) {
				$data = [
					'gender' => $gender,
					'nama' => $nama,
					'email' => $email,
					'no_hp' => $no_hp,
				];

				$where = ['id' => $id,];

				$this->service->update('mahasiswa', $where, $data);
				echo $this->service->successResponse([], 'Update data mahasiswa berhasil!');
			}
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}

	public function mahasiswa_delete() {
		try {
			$id = $this->input->post('id');

			$default = $this->service->detail('mahasiswa', 'id', $id);

			$whereUser = ['id' => $default['user_id']];
			$whereAdmin = ['id' => $id];

			$this->service->delete('user',$whereUser);
			$this->service->delete('mahasiswa',$whereAdmin);
			
			echo $this->service->successResponse([], 'Data mahasiswa berhasil di hapus!');
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}
	// End Of Mahasiswa

	// Jurusan
	public function jurusan_list() {
		try {
			$kampus_id = $this->input->get('kampus_id');
			$condition = [];

			if ($kampus_id !== null) {
				$condition['kampus_id'] = $kampus_id;
			}

			$jurusan = $this->service->get('jurusan', $condition);

			echo $this->service->successResponse($jurusan);
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}

	public function jurusan_add() {
		try {
			$id = $this->input->post('id');
			$nama = $this->input->post('nama');
			$kampus_id = $this->input->post('kampus_id');

			$data = [
				'id' => $id,
				'nama' => $nama,
				'kampus_id' => $kampus_id,
			];

			$this->service->insert('jurusan', $data);
			echo $this->service->successResponse([], 'Tambah data jurusan berhasil!');
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}

	public function jurusan_update() {
		try {
			$id = $this->input->post('id');

			$default = $this->service->detail('jurusan', 'id', $id);

			$nama = $this->input->post('nama') ?? $default['nama'];

			$data = [
				'nama' => $nama,
			];

			$where = ['id' => $id];

			$this->service->update('jurusan',$where, $data);
			echo $this->service->successResponse([], 'Update data jurusan berhasil!');
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}

	public function jurusan_delete() {
		try {
			$id = $this->input->post('id');

			$where = ['id' => $id,];

			$this->service->delete('jurusan',$where);
			
			echo $this->service->successResponse([], 'Data jurusan berhasil di hapus!');
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}
	// End Of Jurusan

	// Mata Kuliah
	public function mata_kuliah_list() {
		try {
			$kampus_id = $this->input->get('kampus_id');

			if ($kampus_id !== null) {
				$this->db->where('mata_kuliah.kampus_id', $kampus_id);
			}

			$this->db->select('*, mata_kuliah.id as `id`, mata_kuliah.nama as `nama`, jurusan.nama as `jurusan`', FALSE);
			$this->db->from('mata_kuliah');
			$this->db->join('jurusan', 'jurusan.id = mata_kuliah.jurusan_id', 'inner');
			$query = $this->db->get();

			$mata_kuliahs = $query->result_array();

			echo $this->service->successResponse($mata_kuliahs);
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}

	public function mata_kuliah_add() {
		try {
			$id = $this->input->post('id');
			$nama = $this->input->post('nama');
			$jurusan_id = $this->input->post('jurusan_id');
			$kampus_id = $this->input->post('kampus_id');

			$data = [
				'id' => $id,
				'nama' => $nama,
				'jurusan_id' => $jurusan_id,
				'kampus_id' => $kampus_id,
			];

			$this->service->insert('mata_kuliah', $data);
			echo $this->service->successResponse([], 'Tambah data mata kuliah berhasil!');
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}

	public function mata_kuliah_update() {
		try {
			$id = $this->input->post('id');

			$default = $this->service->detail('mata_kuliah', 'id', $id);

			$nama = $this->input->post('nama') ?? $default['nama'];
			$jurusan_id = $this->input->post('jurusan_id') ?? $default['jurusan_id'];

			$data = [
				'nama' => $nama,
				'jurusan_id' => $jurusan_id,
			];

			$where = ['id' => $id];

			$this->service->update('mata_kuliah',$where, $data);
			echo $this->service->successResponse([], 'Update data mata kuliah berhasil!');
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}

	public function mata_kuliah_delete() {
		try {
			$id = $this->input->post('id');

			$where = ['id' => $id,];

			$this->service->delete('mata_kuliah',$where);
			
			echo $this->service->successResponse([], 'Data mata kuliah berhasil di hapus!');
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}
	// End Of Mata Kuliah

	// Dosen Mengajar
	public function dosen_mengajar_list() {
		try {
			$kampus_id = $this->input->get('kampus_id');
			$mata_kuliah_id = $this->input->get('mata_kuliah_id');
			$dosen_id = $this->input->get('dosen_id');

			if ($kampus_id !== null) {
				$this->db->where('dosen_mengajar.kampus_id', $kampus_id);
			}

			if ($mata_kuliah_id !== null) {
				$this->db->where('dosen_mengajar.mata_kuliah_id', $mata_kuliah_id);
			}

			if ($dosen_id !== null) {
				$this->db->where('dosen_mengajar.dosen_id', $dosen_id);
			}

			$this->db->select('*, dosen_mengajar.id as `id`, mata_kuliah.nama as `mata_kuliah`, dosen.nama as `dosen`', FALSE);
			$this->db->from('dosen_mengajar');
			$this->db->join('mata_kuliah', 'mata_kuliah.id = dosen_mengajar.mata_kuliah_id', 'inner');
			$this->db->join('dosen', 'dosen.id = dosen_mengajar.dosen_id', 'inner');
			$query = $this->db->get();

			$dosen_mengajars = $query->result_array();

			echo $this->service->successResponse($dosen_mengajars);
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 	
	}

	public function dosen_mengajar_add() {
		try {
			$dosen_id = $this->input->post('dosen_id');
			$mata_kuliah_id = $this->input->post('mata_kuliah_id');
			$kampus_id = $this->input->post('kampus_id');

			$data = [
				'dosen_id' => $dosen_id,
				'mata_kuliah_id' => $mata_kuliah_id,
				'kampus_id' => $kampus_id,
			];

			$this->service->insert('dosen_mengajar', $data);
			echo $this->service->successResponse([], 'Tambah data tugas dosen berhasil!');
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}

	public function dosen_mengajar_update() {
		try {
			$id = $this->input->post('id');

			$default = $this->service->detail('dosen_mengajar', 'id', $id);

			$dosen_id = $this->input->post('dosen_id') ?? $default['dosen_id'];
			$mata_kuliah_id = $this->input->post('mata_kuliah_id') ?? $default['mata_kuliah_id'];

			$data = [
				'dosen_id' => $dosen_id,
				'mata_kuliah_id' => $mata_kuliah_id,
			];

			$where = ['id' => $id];

			$this->service->update('dosen_mengajar',$where, $data);
			echo $this->service->successResponse([], 'Update data tugas dosen berhasil!');
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}

	public function dosen_mengajar_delete() {
		try {
			$id = $this->input->post('id');

			$where = ['id' => $id,];

			$this->service->delete('dosen_mengajar',$where);
			
			echo $this->service->successResponse([], 'Data tugas kuliah berhasil di hapus!');
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}
	// End Of Dosen Mengajar

	// Dosen Angkatan Mahasiswa
	public function mahasiswa_angkatan_list() {
		try {
			$kampus_id = $this->input->get('kampus_id');

			if ($kampus_id !== null) {
				$this->db->where('mahasiswa_angkatan.kampus_id', $kampus_id);
			}

			$this->db->select('*, mahasiswa_angkatan.id as `id`, jurusan.nama as `jurusan`, mahasiswa.nama as `mahasiswa`', FALSE);
			$this->db->from('mahasiswa_angkatan');
			$this->db->join('mahasiswa', 'mahasiswa.id = mahasiswa_angkatan.mahasiswa_id', 'inner');
			$this->db->join('jurusan', 'jurusan.id = mahasiswa_angkatan.jurusan_id', 'inner');
			$query = $this->db->get();

			$mahasiswa_angkatans = $query->result_array();

			echo $this->service->successResponse($mahasiswa_angkatans);
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 	
	}

	public function mahasiswa_angkatan_add() {
		try {
			$angkatan = $this->input->post('angkatan');
			$mahasiswa_id = $this->input->post('mahasiswa_id');
			$jurusan_id = $this->input->post('jurusan_id');
			$kampus_id = $this->input->post('kampus_id');

			$data = [
				'angkatan' => $angkatan,
				'mahasiswa_id' => $mahasiswa_id,
				'jurusan_id' => $jurusan_id,
				'kampus_id' => $kampus_id,
			];

			$this->service->insert('mahasiswa_angkatan', $data);
			echo $this->service->successResponse([], 'Tambah angkatan mahasiswa berhasil!');
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}

	public function mahasiswa_angkatan_update() {
		try {
			$id = $this->input->post('id');

			$default = $this->service->detail('mahasiswa_angkatan', 'id', $id);

			$angkatan = $this->input->post('angkatan' ?? $default['angkatan']);
			$mahasiswa_id = $this->input->post('mahasiswa_id') ?? $default['mahasiswa_id'];
			$jurusan_id = $this->input->post('jurusan_id') ?? $default['jurusan_id'];

			$data = [
				'angkatan' => $angkatan,
				'mahasiswa_id' => $mahasiswa_id,
				'jurusan_id' => $jurusan_id,
			];

			$where = ['id' => $id];

			$this->service->update('mahasiswa_angkatan',$where, $data);
			echo $this->service->successResponse([], 'Update angkatan mahasiswa berhasil!');
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}

	public function mahasiswa_angkatan_delete() {
		try {
			$id = $this->input->post('id');

			$where = ['id' => $id,];

			$this->service->delete('mahasiswa_angkatan',$where);
			
			echo $this->service->successResponse([], 'Data angkatan mahasiswa berhasil di hapus!');
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}
	// End Of Angkatan Mahasiswa

	// Jadwal
	public function jadwal_list() {
		try {
			$kampus_id = $this->input->get('kampus_id');

			if ($kampus_id !== null) {
				$this->db->where('jadwal.kampus_id', $kampus_id);
			}

			$this->db->select('*, jadwal.id as `id`, mata_kuliah.nama as `mata_kuliah`, dosen.nama as `dosen`, dosen_mengajar.mata_kuliah_id as `mata_kuliah_id`, dosen_mengajar.dosen_id as `dosen_id`', FALSE);
			$this->db->from('jadwal');
			$this->db->join('dosen_mengajar', 'dosen_mengajar.id = jadwal.dosen_mengajar_id', 'inner');
			$this->db->join('mata_kuliah', 'mata_kuliah.id = dosen_mengajar.mata_kuliah_id', 'inner');
			$this->db->join('dosen', 'dosen.id = dosen_mengajar.dosen_id', 'inner');
			$this->db->order_by('jadwal.id', 'DESC');
			$query = $this->db->get();

			$jadwals = $query->result_array();

			echo $this->service->successResponse($jadwals);
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 	
	}

	public function jadwal_add() {
		try {
			$waktu_mulai = $this->input->post('waktu_mulai');
			$waktu_selesai = $this->input->post('waktu_selesai');
			$dosen_mengajar_id = $this->input->post('dosen_mengajar_id');
			$kampus_id = $this->input->post('kampus_id');

			$data = [
				'waktu_mulai' => $waktu_mulai,
				'waktu_selesai' => $waktu_selesai,
				'dosen_mengajar_id' => $dosen_mengajar_id,
				'kampus_id' => $kampus_id,
			];

			$this->service->insert('jadwal', $data);
			echo $this->service->successResponse([], 'Tambah jadwal berhasil!');
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}

	public function jadwal_update() {
		try {
			$id = $this->input->post('id');

			$default = $this->service->detail('jadwal', 'id', $id);

			$waktu_mulai = $this->input->post('waktu_mulai') ?? $default['waktu_mulai'];
			$waktu_selesai = $this->input->post('waktu_selesai') ?? $default['waktu_selesai'];
			$dosen_mengajar_id = $this->input->post('dosen_mengajar_id') ?? $default['dosen_mengajar_id'];

			$data = [
				'waktu_mulai' => $waktu_mulai,
				'waktu_selesai' => $waktu_selesai,
				'dosen_mengajar_id' => $dosen_mengajar_id,
			];

			$where = ['id' => $id];

			$this->service->update('jadwal',$where, $data);
			echo $this->service->successResponse([], 'Update jadwal berhasil!');
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}

	public function jadwal_delete() {
		try {
			$id = $this->input->post('id');

			$where = ['id' => $id,];

			$this->service->delete('jadwal',$where);
			
			echo $this->service->successResponse([], 'Data jadwal berhasil di hapus!');
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}
	// End Of Jadwal

}
