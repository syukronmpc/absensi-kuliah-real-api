<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {

	public function jadwal_list() {
		try {
			$kampus_id = $this->input->get('kampus_id');
			$all = $this->input->get('show_all');

			$now = date('Y-m-d H:i:s');

			if ($all !== 'true') {
				$this->db->where("waktu_mulai <=' ".$now."'");
				$this->db->where("waktu_selesai >= '".$now."'");
			}

			if ($kampus_id !== null) {
				$this->db->where('jadwal.kampus_id', $kampus_id);
			}

			$this->db->select('*, jadwal.id as `id`, mata_kuliah.nama as `mata_kuliah`, dosen.nama as `dosen`, dosen_mengajar.mata_kuliah_id as `mata_kuliah_id`, dosen_mengajar.dosen_id as `dosen_id`, jurusan.nama as `jurusan`', FALSE);
			$this->db->from('jadwal');
			$this->db->join('dosen_mengajar', 'dosen_mengajar.id = jadwal.dosen_mengajar_id', 'inner');
			$this->db->join('mata_kuliah', 'mata_kuliah.id = dosen_mengajar.mata_kuliah_id', 'inner');
			$this->db->join('jurusan', 'jurusan.id = mata_kuliah.jurusan_id', 'inner');
			$this->db->join('dosen', 'dosen.id = dosen_mengajar.dosen_id', 'inner');
			$this->db->order_by('waktu_mulai', 'DESC');
			$query = $this->db->get();

			$jadwals = $query->result_array();

			echo $this->service->successResponse($jadwals);
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 	
	}

	public function daftar_hadir_add() {
		try {
			$jadwal_id = $this->input->post('jadwal_id');
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$kampus_id = $this->input->post('kampus_id');

			$condition = [
				'username' => $username,
				'password' => $password,
				'akses' => $this->service->akses['MAHASISWA'],
			];

			$user = $this->service->get('user', $condition);

			if (empty($user)) {
				throw new Exception('Username atau password salah');
			}

			$user_id = $user[0]['id'];
			$mahasiswa = $this->service->detail('mahasiswa', 'user_id', $user_id);	

			if (!$mahasiswa) {
				throw new Exception('Username atau password salah');
			}

			if ($kampus_id != $mahasiswa['kampus_id']) {
				throw new Exception('Username atau password salah');
			}

			$conditionAngkatan = [
				'mahasiswa_id' => $mahasiswa['id']
			];

			$angkatan = $this->service->get('mahasiswa_angkatan', $conditionAngkatan);

			if (count($angkatan) < 1) {
				throw new Exception('Username atau password salah');
			}

			$jurusan_id = $angkatan[0]['jurusan_id'];

			// Check Jurusan
			$this->db->select('*, jadwal.id as `id`, mata_kuliah.nama as `mata_kuliah`, dosen.nama as `dosen`, dosen_mengajar.mata_kuliah_id as `mata_kuliah_id`, dosen_mengajar.dosen_id as `dosen_id`', FALSE);
			$this->db->from('jadwal');
			$this->db->join('dosen_mengajar', 'dosen_mengajar.id = jadwal.dosen_mengajar_id', 'inner');
			$this->db->join('mata_kuliah', 'mata_kuliah.id = dosen_mengajar.mata_kuliah_id', 'inner');
			$this->db->join('dosen', 'dosen.id = dosen_mengajar.dosen_id', 'inner');
			$this->db->where('mata_kuliah.jurusan_id', $jurusan_id);
			$this->db->where('jadwal.kampus_id', $kampus_id);
			$this->db->where('jadwal.id', $jadwal_id);
			$query = $this->db->get();
			$jurusan = $query->result_array();

			if (count($jurusan) < 1) {
				throw new Exception('Anda tidak termasuk pada jurusan ini.');
			}
			//

			$daftarHadir = [
				'mahasiswa_id' => $mahasiswa['id'],
				'jadwal_id' => $jadwal_id,
			];

			$dh = $this->service->get('daftar_hadir', $daftarHadir);

			if (count($dh) > 0 ) {
				throw new Exception('Anda sudah absen.');
			}

			$data = [
				'jadwal_id' => $jadwal_id,
				'mahasiswa_id' => $mahasiswa['id'],
				'kampus_id' => $mahasiswa['kampus_id'],
				'waktu_hadir' => date('Y-m-d H:i'),
			];

			$this->service->insert('daftar_hadir', $data);

			echo $this->service->successResponse([], 'Anda berhasil melakukan absensi.');	

		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		}
	}
}
