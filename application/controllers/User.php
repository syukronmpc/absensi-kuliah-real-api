<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function login() {
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$condition = [
			'username' => $username,
			'password' => $password,
		];

		try {
			$user = $this->service->get('user', $condition);

			if (empty($user)) {
				throw new Exception('Username atau password salah');
			}

			$user_id = $user[0]['id'];
			$username = $user[0]['username'];
			$akses = $user[0]['akses'];
			$userData = [
				'id' => $user_id,
				'username' => $username,
				'akses' => $akses,
			];

			if ($akses == $this->service->akses['SUPER ADMIN']) {
				$sa = $this->service->detail('super_admin', 'user_id', $user_id);
				if (!$sa) {
					throw new Exception('Username atau password salah');
				} 
				$userData['identitas'] = $sa; 
			}

			if ($akses == $this->service->akses['ADMIN']) {
				$admin = $this->service->detail('admin', 'user_id', $user_id);
				if (!$admin) {
					throw new Exception('Username atau password salah');
				} 
				$userData['identitas'] = $admin;
				$kampus = $this->service->detail('kampus', 'id', $admin['kampus_id']);
				$userData['kampus_id'] = $kampus['id'];
				$userData['kampus'] = $kampus['nama'];
			}

			if ($akses == $this->service->akses['DOSEN']) {
				$dosen = $this->service->detail('dosen', 'user_id', $user_id);
				if (!$dosen) {
					throw new Exception('Username atau password salah');
				} 
				$userData['identitas'] = $dosen;	
				$kampus = $this->service->detail('kampus', 'id', $dosen['kampus_id']);
				$userData['kampus_id'] = $kampus['id'];
				$userData['kampus'] = $kampus['nama'];
			}

			if ($akses == $this->service->akses['MAHASISWA']) {
				$mahasiswa = $this->service->detail('mahasiswa', 'user_id', $user_id);	
				if (!$mahasiswa) {
					throw new Exception('Username atau password salah');
				} 
				$userData['identitas'] = $mahasiswa;	
				$kampus = $this->service->detail('kampus', 'id', $mahasiswa['kampus_id']);
				$userData['kampus_id'] = $kampus['id'];
				$userData['kampus'] = $kampus['nama'];
			}

			echo $this->service->successResponse($userData);	

		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		}
	}
}
