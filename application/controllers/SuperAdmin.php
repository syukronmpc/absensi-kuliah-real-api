<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SuperAdmin extends CI_Controller {

	// Kampus
	public function kampus_list() {
		try {
			$kampus = $this->service->get('kampus');
			echo $this->service->successResponse($kampus);
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}

	public function kampus_add() {
		try {
			$nama = $this->input->post('nama');
			$alamat = $this->input->post('alamat');

			$data = [
				'nama' => $nama,
				'alamat' => $alamat,
			];

			$this->service->insert('kampus', $data);
			echo $this->service->successResponse([], 'Tambah data kampus berhasil!');
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}

	public function kampus_update() {
		try {
			$id = $this->input->post('id');

			$default = $this->service->detail('kampus', 'id', $id);

			$nama = $this->input->post('nama') ?? $default['nama'];
			$alamat = $this->input->post('alamat') ?? $default['alamat'];

			$data = [
				'nama' => $nama,
				'alamat' => $alamat,
			];

			$where = ['id' => $id,];

			$this->service->update('kampus',$where, $data);
			echo $this->service->successResponse([], 'Update data kampus berhasil!');
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}

	public function kampus_delete() {
		try {
			$id = $this->input->post('id');

			$where = ['id' => $id,];

			$this->service->delete('kampus',$where);
			
			echo $this->service->successResponse([], 'Data kampus berhasil di hapus!');
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}
	// End Of Kampus


	// Admin
	public function admin_list() {
		try {
			$this->db->select('*, admin.id as `id`, user.password, admin.nama as `nama`, kampus.nama as `kampus`', FALSE);
			$this->db->from('admin');
			$this->db->join('kampus', 'kampus.id = admin.kampus_id', 'inner');
			$this->db->join('user', 'user.id = admin.user_id', 'inner');
			$query = $this->db->get();

			$admins = $query->result_array();

			echo $this->service->successResponse($admins);
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}

	public function admin_add() {
		try {
			$id = $this->input->post('id');
			$kampus_id = $this->input->post('kampus_id');
			$password = $this->input->post('password');
			$gender = $this->input->post('gender');
			$nama = $this->input->post('nama');
			$email = $this->input->post('email');
			$no_hp = $this->input->post('no_hp');

			$userdata = [
				'username' => $id,
				'password' => $password,
				'akses' => $this->service->akses['ADMIN'],
			];

			$insert = $this->service->insert('user', $userdata);

			if ($insert) {
				$data = [
					'id' => $id,
					'kampus_id' => $kampus_id,
					'gender' => $gender,
					'nama' => $nama,
					'email' => $email,
					'no_hp' => $no_hp,
					'user_id' => $this->db->insert_id()
				];

				$this->service->insert('admin', $data);
				echo $this->service->successResponse([], 'Tambah data admin berhasil!');
			}
			
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}

	public function admin_update() {
		try {
			$id = $this->input->post('id');

			$default = $this->service->detail('admin', 'id', $id);

			$kampus_id = $this->input->post('kampus_id') ?? $default['kampus_id'];
			$password = $this->input->post('password') ?? $default['password'];
			$gender = $this->input->post('gender') ?? $default['gender'];
			$nama = $this->input->post('nama') ?? $default['nama'];
			$email = $this->input->post('email') ?? $default['email'];
			$no_hp = $this->input->post('no_hp') ?? $default['no_hp'];

			$userdata = [
				'password' => $password,
			];

			$whereUser = ['id' => $default['user_id']];

			$update = $this->service->update('user', $whereUser, $userdata);

			if ($update) {
				$data = [
					'kampus_id' => $kampus_id,
					'gender' => $gender,
					'nama' => $nama,
					'email' => $email,
					'no_hp' => $no_hp,
				];

				$where = ['id' => $id,];

				$this->service->update('admin', $where, $data);
				echo $this->service->successResponse([], 'Update data admin berhasil!');
			}
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}

	public function admin_delete() {
		try {
			$id = $this->input->post('id');

			$default = $this->service->detail('admin', 'id', $id);

			$whereUser = ['id' => $default['user_id']];
			$whereAdmin = ['id' => $id];

			$this->service->delete('user',$whereUser);
			$this->service->delete('admin',$whereAdmin);
			
			echo $this->service->successResponse([], 'Data admin berhasil di hapus!');
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 
	}
}
