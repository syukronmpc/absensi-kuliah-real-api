<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen extends CI_Controller {

	public function jadwal_list() {
		try {
			$kampus_id = $this->input->get('kampus_id');
			$dosen_id = $this->input->get('dosen_id');

			if ($kampus_id !== null) {
				$this->db->where('jadwal.kampus_id', $kampus_id);
			}

			if ($dosen_id !== null) {
				$this->db->where('dosen_mengajar.dosen_id', $dosen_id);
			}

			$this->db->select('*, jadwal.id as `id`, mata_kuliah.nama as `mata_kuliah`, dosen.nama as `dosen`, dosen_mengajar.mata_kuliah_id as `mata_kuliah_id`, dosen_mengajar.dosen_id as `dosen_id`', FALSE);
			$this->db->from('jadwal');
			$this->db->join('dosen_mengajar', 'dosen_mengajar.id = jadwal.dosen_mengajar_id', 'inner');
			$this->db->join('mata_kuliah', 'mata_kuliah.id = dosen_mengajar.mata_kuliah_id', 'inner');
			$this->db->join('dosen', 'dosen.id = dosen_mengajar.dosen_id', 'inner');
			$this->db->order_by('waktu_mulai', 'DESC');
			$query = $this->db->get();

			$jadwals = $query->result_array();

			echo $this->service->successResponse($jadwals);
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		} 	
	}

	public function daftar_hadir_list() {
		try {
			$kampus_id = $this->input->get('kampus_id');
			$jadwal_id = $this->input->get('jadwal_id');

			$this->db->where('daftar_hadir.jadwal_id', $jadwal_id);
			$this->db->where('daftar_hadir.kampus_id', $kampus_id);

			$this->db->select('*, daftar_hadir.id as `id`, mahasiswa.nama as `mahasiswa`', FALSE);
			$this->db->from('daftar_hadir');
			$this->db->join('jadwal', 'jadwal.id = daftar_hadir.jadwal_id', 'inner');
			$this->db->join('mahasiswa', 'mahasiswa.id = daftar_hadir.mahasiswa_id', 'inner');
			$query = $this->db->get();
			$dh = $query->result_array();

			echo $this->service->successResponse($dh);	
		} catch (Exception $error) {
			echo $this->service->failResponse($error->getMessage(), $error->getTrace());
		}
	}
}
