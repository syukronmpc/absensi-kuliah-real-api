<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ServiceModel extends CI_Model {

	public $akses = [
		'SUPER ADMIN' => 'SUPER ADMIN',
		'ADMIN' => 'ADMIN',
		'DOSEN' => 'DOSEN',
		'MAHASISWA' => 'MAHASISWA'
	];

	public function get($table, $where = null)
	{	
		if ($where !== null ){
			$this->db->where($where);
		}

		$query = $this->db->get($table);

		return $query->result_array();
	}

	public function detail($table, $field, $where)
	{
		$this->db->where([$field => $where]);
		$query = $this->db->get($table);

		return $query->row_array();
	}

	public function insert($table, $data)
	{
		$query = $this->db->insert($table, $data);
		return $query;
	}

	public function update($table, $where, $data)
	{
		$query = $this->db->update($table, $data, $where);
		return $query;
	}

	public function delete($table, $where)
	{
		$query = $this->db->delete($table, $where);
		return $query;
	}

	public function successResponse($data, $message = 'success') {
		$response = [
			'status' => 1,
			'message' => $message,
			'data' => $data,
		];

		return json_encode($response);
	}

	public function failResponse($message, $errors = []) {
		$response = [
			'status' => 0,
			'message' => $message,
			'errors' => $errors,
		];

		return json_encode($response);
	}
}
